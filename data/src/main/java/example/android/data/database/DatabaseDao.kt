package example.android.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import example.android.data.model.PhotoEntity
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface DatabaseDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllPhotos(photos: List<PhotoEntity>): Completable

    @Query("SELECT * from photos_table")
    fun getAllPhotos(): Flowable<List<PhotoEntity>>

}

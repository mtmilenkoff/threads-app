package example.android.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import example.android.data.model.PhotoEntity

@Database(entities = [PhotoEntity::class], version = 1, exportSchema = false)
abstract class MainDatabase : RoomDatabase() {
    abstract val myDao: DatabaseDao
}

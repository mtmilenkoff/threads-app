package example.android.data.database

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {
    @Singleton
    @Provides
    fun provideDatabase(app: Application): MainDatabase {
        return Room.databaseBuilder(
            app.applicationContext,
            MainDatabase::class.java,
            "database"
        ).build()
    }
    @Singleton
    @Provides
    fun provideDao(database : MainDatabase): DatabaseDao {
        return database.myDao
    }
}
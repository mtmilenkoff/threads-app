package example.android.data.network

import example.android.data.model.PhotoEntity
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService{
    @GET("photos")
    fun getPhotos(@Query("id") ids: List<Int>) : Single<List<PhotoEntity>>
}

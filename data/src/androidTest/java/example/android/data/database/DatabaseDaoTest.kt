package example.android.data.database

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import example.android.data.model.PhotoEntity
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.arch.core.executor.testing.*
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class DatabaseDaoTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: MainDatabase
    private lateinit var dao: DatabaseDao

    @Before
    fun init(){
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            MainDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.myDao
    }

    @Test
    fun insertAll_Equals_GetAll_Test() { runBlocking {
        //GIVEN a list of objects
        val photo1 = PhotoEntity(1,111,"title1","url1.com","thumbnail1")
        val photo2 = PhotoEntity(2,222,"title2","url2.com","thumbnail2")
        val dummyList = listOf(photo1, photo2)

        //WHEN we insert them into the database
        dao.insertAllPhotos(dummyList).subscribe().dispose()
        val tester = dao.getAllPhotos().test()

        //THEN the list retrieved from the database and the original list are the same
        tester.assertNoErrors()
        tester.assertValueCount(1)
        assertThat(dao.getAllPhotos().blockingFirst()).isEqualTo(dummyList)
        }
    }

    @After
    fun close(){
        database.close()
    }
}
package example.android.threadsapp.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import example.android.threadsapp.fakes.FakeUseCaseGetPhotos
import example.android.threadsapp.fakes.FakeUseCaseUpdatePhotos
import org.junit.Rule
import org.junit.Test

/*
 class MainViewModelTest{

    @get:Rule
    val rule = InstantTaskExecutorRule()

    var repo = FakePhotosRepository()
    var getPhotos = FakeUseCaseGetPhotos(repo)
    var updatePhotos = FakeUseCaseUpdatePhotos(repo)
    val viewmodel = MainViewModel(updatePhotos,getPhotos)

    @Test
    fun updateRepo() {
        //GIVEN a repository with an empty database
        assertThat(repo.getPhotos()).isEmpty()

        //WHEN we call the update method
        viewmodel.updateRepo()

        //THEN the repository database is updated
        assertThat(repo.getPhotos()).isNotEmpty()
    }

    @Test
    fun useCase_Returns_Correctly(){
        //GIVEN a repository and a getPhotos usecase
        repo.updateDatabase()

        //WHEN we call the get usecase
        //THEN the result we get is the same as the repository
        assertThat(repo.getPhotos()).isEqualTo(getPhotos())
    }
}
*/
package example.android.threadsapp.fakes

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import example.android.data.model.PhotoEntity
import example.android.domain.PhotosRepositoryInterface
import example.android.domain.model.PhotoModel
import example.android.domain.model.toListModel

/*
class FakePhotosRepository : PhotosRepositoryInterface {

    private val photosList = mutableListOf<PhotoEntity>()
    private val apiResponse = listOf(
        PhotoEntity(1,111,"title1","url1.com","thumbnail1"),
        PhotoEntity(2,222,"title2","url2.com","thumbnail2"),
        PhotoEntity(3,333,"title3","url3.com","thumbnail3"),
        PhotoEntity(4,444,"title4","url4.com","thumbnail4"),
        PhotoEntity(5,555,"title5","url5.com","thumbnail5"),
        PhotoEntity(6,666,"title6","url6.com","thumbnail6"),
    )

    override suspend fun updateDatabase(){
        photosList.addAll(apiResponse)
    }

     override fun getPhotos(): LiveData<List<PhotoModel>> {
        return liveData{
            emit(apiResponse)
        }
    }
}
*/
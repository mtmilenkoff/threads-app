package example.android.threadsapp.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import dagger.hilt.android.AndroidEntryPoint
import example.android.domain.model.toListModel
import example.android.threadsapp.R
import example.android.threadsapp.databinding.ActivityMainBinding
import example.android.threadsapp.ui.recycleview.PhotoViewAdapter
import example.android.threadsapp.ui.viewmodel.MainViewModel
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

@AndroidEntryPoint
class MainActivity: AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        disposable.clear()

        //Get viewmodel and binding
        val viewModel : MainViewModel by viewModels()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        //Set up adapter on recyclerview
        val adapter = PhotoViewAdapter()
        binding.recyclerView.adapter = adapter


        disposable.add(viewModel.photosList
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{adapter.submitList(it)})


        viewModel.isUpdating.observe(this, { binding.swipeRefresh.isRefreshing = it })
        binding.swipeRefresh.setOnRefreshListener { viewModel.updateRepo() }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }
}
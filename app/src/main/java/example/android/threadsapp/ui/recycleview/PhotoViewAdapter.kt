package example.android.threadsapp.ui.recycleview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import example.android.domain.model.PhotoModel
import example.android.threadsapp.databinding.RecycleCardPhotoBinding

class PhotoViewAdapter : ListAdapter<PhotoModel, PhotoViewHolder>(PhotoDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RecycleCardPhotoBinding.inflate(layoutInflater, parent, false)
        return PhotoViewHolder(binding)
    }
    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val photo = getItem(position)
        holder.bind(photo)
    }
}
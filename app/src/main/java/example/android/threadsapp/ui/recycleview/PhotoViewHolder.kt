package example.android.threadsapp.ui.recycleview

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import example.android.threadsapp.ui.activities.DetailActivity
import example.android.threadsapp.R
import example.android.domain.model.PhotoModel
import example.android.threadsapp.databinding.RecycleCardPhotoBinding

class PhotoViewHolder(binding: RecycleCardPhotoBinding) : RecyclerView.ViewHolder(binding.root) {
    private val title: TextView = binding.imageTitle
    private val image: ImageView = binding.imagePreview
    private val card: ConstraintLayout = binding.card

    fun bind(photo: PhotoModel){
        title.text = photo.title
        card.setOnClickListener{
            val intent = Intent(it.context, DetailActivity::class.java)
            intent.putExtra("photoUrl",photo.url)
            it.context.startActivity(intent)
        }
        val url = photo.thumbnailUrl+".png"
        Glide.with(image.context)
            .load(url)
            .apply(
                RequestOptions()
                .placeholder(R.drawable.ic_baseline_keyboard_arrow_down_24)
                .error(R.drawable.ic_baseline_broken_image_24))
            .into(image)
    }
}
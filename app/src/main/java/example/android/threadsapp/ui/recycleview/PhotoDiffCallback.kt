package example.android.threadsapp.ui.recycleview

import androidx.recyclerview.widget.DiffUtil
import example.android.domain.model.PhotoModel

class PhotoDiffCallback : DiffUtil.ItemCallback<PhotoModel>() {
    override fun areItemsTheSame(oldItem: PhotoModel, newItem: PhotoModel): Boolean {
        return oldItem === newItem
    }
    override fun areContentsTheSame(oldItem: PhotoModel, newItem: PhotoModel): Boolean {
        return oldItem.id == newItem.id
    }
}
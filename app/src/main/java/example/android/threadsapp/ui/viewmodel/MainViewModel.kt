package example.android.threadsapp.ui.viewmodel

import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import example.android.domain.usecase.UseCaseGetPhotos
import example.android.domain.usecase.UseCaseUpdatePhotos
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val useCaseUpdatePhotos : UseCaseUpdatePhotos,
    private val useCaseGetPhotos : UseCaseGetPhotos) : ViewModel() {

    private val _isUpdating = MutableLiveData(false)
    val isUpdating: LiveData<Boolean> get() = _isUpdating
    var photosList = useCaseGetPhotos()

    init { updateRepo() }

    fun updateRepo(){
        viewModelScope.launch {
            _isUpdating.value = true
            useCaseUpdatePhotos()
            photosList = useCaseGetPhotos()
            _isUpdating.value = false
        }
    }
}
package example.android.domain

import androidx.lifecycle.LiveData
import example.android.data.model.PhotoEntity
import example.android.domain.model.PhotoModel
import io.reactivex.Flowable

interface PhotosRepositoryInterface {
    suspend fun updateDatabase()
    fun getPhotos(): Flowable<List<PhotoEntity>>
}
package example.android.domain.usecase

import example.android.domain.PhotosRepository
import example.android.domain.PhotosRepositoryInterface
import javax.inject.Inject

class UseCaseUpdatePhotos @Inject constructor(private val repo : PhotosRepository) {
    operator fun invoke() { repo.updateDatabase() }
}
package example.android.domain.usecase

import example.android.domain.PhotosRepository
import javax.inject.Inject

class UseCaseGetPhotos @Inject constructor(private val repo : PhotosRepository) {
    operator fun invoke() = repo.getPhotos()
}
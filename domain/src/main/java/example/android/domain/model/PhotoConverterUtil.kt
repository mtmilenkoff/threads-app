package example.android.domain.model

import example.android.data.model.PhotoEntity

fun PhotoEntity.toModel() : PhotoModel = PhotoModel(this.albumId, this.id, this.title, this.url, this.thumbnailUrl)
fun PhotoModel.toEntity() : PhotoEntity = PhotoEntity(this.albumId, this.id, this.title, this.url, this.thumbnailUrl)

fun List<PhotoEntity>.toListModel() : List<PhotoModel> = this.map { e -> e.toModel() }
fun List<PhotoModel>.toListEntity() : List<PhotoEntity> = this.map { e -> e.toEntity() }

package example.android.domain

import android.util.Log
import example.android.data.database.DatabaseDao
import example.android.data.network.ApiService
import example.android.domain.model.PhotoModel
import example.android.domain.model.toListModel
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PhotosRepository  @Inject constructor(private val dao: DatabaseDao, private val apiService: ApiService) {

    private val disposer = CompositeDisposable()

    fun getPhotos() : Flowable<List<PhotoModel>> = dao.getAllPhotos().map { it.toListModel() }

    fun updateDatabase() {
        //Get photos from API and insert them into DB
        val resultCall = apiService.getPhotos((1..25).toList())
        disposer.add(resultCall.subscribeOn(Schedulers.io())
            .subscribe(
                { dao.insertAllPhotos(it).subscribe(
                    {
                        Log.i("REPOSITORY", "INSERTED SUCCESSFULLY")
                        disposing()
                    },
                    {Log.e("REPOSITORY", "FAILED TO INSERT")}
                ).dispose() },
                { Log.e("REPOSITORY", "ERROR") }
            )
        )
    }

    private fun disposing(){
        disposer.dispose()
    }
}
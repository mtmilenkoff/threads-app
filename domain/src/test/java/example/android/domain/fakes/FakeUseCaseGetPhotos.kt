package example.android.domain.fakes

import example.android.domain.usecase.FakePhotosRepository
import javax.inject.Inject

class FakeUseCaseGetPhotos @Inject constructor(private val repo : FakePhotosRepository) {
    operator fun invoke() = repo.getPhotos()
}
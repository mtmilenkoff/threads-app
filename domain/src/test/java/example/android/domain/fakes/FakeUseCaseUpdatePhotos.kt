package example.android.domain.fakes

import example.android.domain.PhotosRepositoryInterface
import example.android.domain.usecase.FakePhotosRepository
import javax.inject.Inject

class FakeUseCaseUpdatePhotos @Inject constructor(private val repo : PhotosRepositoryInterface) {
    operator fun invoke() = repo.getPhotos()
}